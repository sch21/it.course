onload = () => {
    const canvas = document.createElement('canvas');
    document.body.appendChild(canvas);
    const Button = document.getElementById('Button');

    function process(){
        var first=prompt("e_i");
        var second=prompt("e_j");
        console.log("e_i = ",first)
        console.log("e_j = ",second)
        console.log("e_ij = ",bubble(first+second))
    }

    function bubble(ans){
        let array = ('' + ans).split('')
        let N = array.length
        let count = 0
        let num = ''
        let X = 0

        for (i = 0; i < N-1; i++){
            for (j = 0; j < N-1-i; j++){
                if (array[j] > array[j+1]){
                    count++
                    X = array[j+1]
                    array[j+1] = array[j]
                    array[j] = X
                }
            }
        }
        if(count%2!=0){num='-'}

        for (v = 0; v < N; v++){
            if (array[v]!= array[v+1]){
                num += String(array[v])
            }
            else{
                v++
            }
        }

        return num
    }

    Button.addEventListener('click', process);

};