onload = () => {
    const canvas = document.createElement('canvas');
    const ctx = canvas.getContext('2d');
  
    canvas.width = 600;
    canvas.height = 600;


    function field () {
        ctx.setTransform()
        ctx.fillStyle = 'black'
        ctx.fillRect(0,0,canvas.width, canvas.height);


        ctx.fillStyle="#757060";
        ctx.arc(300, 300, 290, 0, 2*Math.PI, false);
        ctx.fill();  

        ctx.fillStyle ="white"
        ctx.font = "20px Courier";
        ctx.fillText("12", 295, 50);
        ctx.fillText("1", 425, 90);
        ctx.fillText("2", 500, 175);
        ctx.fillText("3", 540, 300);
        ctx.fillText("4", 500, 435);
        ctx.fillText("5", 420, 530);
        ctx.fillText("6", 295, 570);
        ctx.fillText("7", 180, 540);
        ctx.fillText("8", 65, 445);
        ctx.fillText("9", 35, 300);
        ctx.fillText("10", 60, 170);
        ctx.fillText("11", 170, 80);

    }

  
    const drawFrame = t => {
        
        field()

        const cos = Math.cos(t/2000)
        const sin = Math.sin(t/2000)

        ctx.fillStyle ="white"

        ctx.setTransform(sin, -cos, cos, sin, canvas.width/2, canvas.height/2);

        
        for (let i = 0; i < 1; i += 1){
            ctx.fillRect(0, 0, 230, 3, - 5*i);
            ctx.transform(1, 0, 0, 1, 50, 1);
        }

        

        const cos2 = Math.cos(t/120000)
        const sin2 = Math.sin(t/120000)

        ctx.fillStyle ="white"
        ctx.setTransform(sin2, -cos2, cos2,sin2, canvas.width/2, canvas.height/2);

        
        for (let i = 0; i < 1; i += 1){
            ctx.fillRect(0, 0, 180, 7, - 5*i);
            ctx.transform(1, 0, 0, 1, 50, 1);
        }
        requestAnimationFrame(drawFrame);
        
    };
    
    requestAnimationFrame(drawFrame);

    document.body.appendChild(canvas)
   
};